package drawableObjects;

import java.awt.Color;

import domain.ShapeBase;
import domain.ShapeType;

public abstract class ColorfulShape extends ShapeBase {
	protected Color outlineColor;
	protected Color fillColor;
	
	static Color defaultFillColor = new Color(240, 240, 240, 96);
	static float colorMixWeight = 0.9f;
	
	public Color getOutlineColor() {
		return outlineColor;
	}
	public Color getFillColor() {
		return fillColor;
	}
	
	protected ColorfulShape() {
		this(0, 0, ShapeType.DEFAULT, new Color(0, 0, 0), new Color(0, 0, 0));
	}
	protected ColorfulShape(int x, int y, ShapeType type, Color outline, Color fill) {
		super(x, y, type);
		this.outlineColor = outline;
		this.fillColor = fill;
		setFillColorAndOpacity();
	}
	
	private void setFillColorAndOpacity() {
		float fillRed = defaultFillColor.getRed() * colorMixWeight;
		float fillGreen = defaultFillColor.getGreen() * colorMixWeight;
		float fillBlue = defaultFillColor.getBlue() * colorMixWeight;
		
		fillRed += outlineColor.getRed() * (1.0f - colorMixWeight);
		fillGreen += outlineColor.getGreen() * (1.0f - colorMixWeight);
		fillBlue += outlineColor.getBlue() * (1.0f - colorMixWeight);
		
		fillColor = new Color((int)(fillRed), (int)(fillGreen), (int)(fillBlue), defaultFillColor.getAlpha());
	}
}
