package drawableObjects;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.geom.AffineTransform;

import domain.ShapeType;
import interfaces.DrawableShape;

public class DrawableOval extends ColorfulShape implements DrawableShape {
	private double rotation;
	
	public double getRotation() {
		return rotation;
	}
	
	public DrawableOval() {
		this(0, 0, 0, 0, 0, new Color(0,0,0));
	}
	public DrawableOval(int x, int y, int width, int height, int rotation, Color outlineColor) {
		this(x, y, width, height, rotation, outlineColor, new Color(0,0,0));
	}
	
	public DrawableOval(int x, int y, int width, int height, int rotation, Color outlineColor, Color fillColor) {
		super(x, y, ShapeType.OVAL_DISPLAY, outlineColor, fillColor);
		this.width = width;
		this.height = height;
		this.rotation = rotation;
	}
	
	@Override
	public void draw(Graphics graphics)
	{
		Graphics2D graphics2D = (Graphics2D)graphics;
		// Make the circle smooth - add antialiasing
		graphics2D.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
		AffineTransform oldTransform;
		int originX, originY;
		originX = this.x - (this.width / 2);
		originY = this.y - (this.width / 2);
		// In order to not stack translations/rotations, we have to store the default transform
		// Then re-apply it at the end of the operation
		// This resets all the translations/rotations we've done on this shape
		// So the next shape is drawn at a null transform
		oldTransform = graphics2D.getTransform();
		
		// Java paints on a transform matrix and considers the top-left of a shape as its origin
		// Therefore, we need to move the shape back to the real origin (centre of the shape) before rotation
		// then reset the position afterwards
		// Also, we cannot paint a shape AT a position - that entire distance vector would be rotated too
		// Instead, we have to move the entire drawing board to the shape's origin, then draw the shape at the new origin
		graphics2D.translate(originX, originY);
		graphics2D.translate(this.width / 2.0, this.width / 2.0);
		graphics2D.rotate(Math.toRadians(this.rotation));
		graphics2D.translate(-this.width / 2.0, -this.height / 2.0);
		
		// Draw the see-through (soft, gooey centre) portion of the oval
		graphics2D.setColor(this.fillColor);
		graphics2D.fillOval(0, 0, this.width, this.height);

		// Draw the outline (crumbly crust) of the oval
		graphics2D.setStroke(new BasicStroke(2));
		graphics2D.setColor(this.outlineColor);
		graphics2D.drawOval(0, 0, this.width, this.height);
		
		// Reset the transform matrix
		graphics2D.setTransform(oldTransform);
	}
}
