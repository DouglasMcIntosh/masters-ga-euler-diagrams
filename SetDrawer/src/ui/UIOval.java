package ui;

import java.awt.Color;

public class UIOval {
	int x;
	int y;
	int width;
	int height;
	double rotation;
	Color outlineColor;
	Color fillColor;
	
	static Color defaultFillColor = new Color(240, 240, 240, 96);
	static float colorMixWeight = 0.9f;
	
	public UIOval(int x, int y, int width, int height, int rotation, Color outlineColor, Color fillColor) {
		this.x = x;
		this.y = y;
		this.width = width;
		this.height = height;
		this.rotation = rotation;
		this.outlineColor = outlineColor;
		this.fillColor = fillColor;
	}
	
	public UIOval(int x, int y, int width, int height, int rotation, Color outlineColor) {
		this.x = x;
		this.y = y;
		this.width = width;
		this.height = height;
		this.rotation = rotation;
		this.outlineColor = outlineColor;

		float fillRed = defaultFillColor.getRed() * colorMixWeight;
		float fillGreen = defaultFillColor.getGreen() * colorMixWeight;
		float fillBlue = defaultFillColor.getBlue() * colorMixWeight;
		
		fillRed += outlineColor.getRed() * (1.0f - colorMixWeight);
		fillGreen += outlineColor.getGreen() * (1.0f - colorMixWeight);
		fillBlue += outlineColor.getBlue() * (1.0f - colorMixWeight);
		
		fillColor = new Color((int)(fillRed), (int)(fillGreen), (int)(fillBlue), defaultFillColor.getAlpha());
	}
}
