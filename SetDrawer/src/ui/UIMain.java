package ui;

import java.awt.Color;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JSeparator;
import javax.swing.JSlider;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.border.EtchedBorder;

import commands.NewShapeCommand;
import interfaces.DrawableShape;

public class UIMain extends JFrame {
	
	UICanvas canvas;
	
	public UIMain(String title, int sizeX, int sizeY) {
		super(title);
		setSize(sizeX, sizeY);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		buildUI();
		setVisible(true);
	}
	
	public void place(DrawableShape shape) {
		canvas.place(shape);
		canvas.repaint();
	}
	
	public void place(List<DrawableShape> shapes) {
		for (DrawableShape shape : shapes)
			canvas.place(shape);
		
		canvas.repaint();
	}
	
	public void clear() {
		canvas.clear();
		canvas.repaint();
	}
	
	public void update() {
		repaint();
	}
	
	private void buildUI() {
		setLayout(new GridBagLayout());
		
		GridBagConstraints gbc = new GridBagConstraints();
		gbc.anchor = GridBagConstraints.PAGE_START;
		gbc.insets = new Insets(6, 6, 6, 6);

		JButton button;
		JLabel label;
		JSlider slider;
		JTextField textField  = new JTextField("Test Text Field");
		canvas = new UICanvas();
		
		
		
		// ---------- Test Textbox
		
		Font textFieldFont = textField.getFont().deriveFont(Font.PLAIN, 30f);
		textField.setFont(textFieldFont);
		gbc.fill = GridBagConstraints.BOTH;
		gbc.anchor = GridBagConstraints.CENTER;
		gbc.weightx = 0.2;
		gbc.weighty = 0;
		gbc.gridwidth = 1;
		gbc.gridheight = 1;
		gbc.gridx = 0;
		gbc.gridy = 0;
		gbc.insets = new Insets(20, 6, 0, 6);
		
		add(textField, gbc);
		
		
		
		// ---------- Test Button
		
		button = new JButton("Test Button");
		
		// Button click listener test
		button.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				RunTextField(textField);
			}
		});
		gbc.fill = GridBagConstraints.HORIZONTAL;
		gbc.weightx = 1.0;
		gbc.weighty = 0;
		gbc.gridwidth = 2;
		gbc.gridheight = 1;
		gbc.gridx = 0;
		gbc.gridy = 1;
		gbc.insets = new Insets(6, 6, 6, 6);
		
		add(button, gbc);
		
		// ---------- Test Label
		
/*		label = new JLabel("Test Slider");
		label.setHorizontalAlignment(JLabel.CENTER);
		gbc.fill = GridBagConstraints.HORIZONTAL;
		gbc.anchor = GridBagConstraints.CENTER;
		gbc.weightx = 0.8;
		gbc.weighty = 0;
		gbc.gridwidth = 1;
		gbc.gridheight = 1;
		gbc.gridx = 0;
		gbc.gridy = 1;
		gbc.insets = new Insets(20, 6, 0, 6);
		
		add(label, gbc);*/
		
		// ---------- Test Slider
		
/*		slider = new JSlider(JSlider.HORIZONTAL, 0, 40, 2);
		slider.setMajorTickSpacing(10);
		slider.setMinorTickSpacing(1);
		slider.setPaintTicks(true);
		slider.setPaintLabels(true);
		slider.setSnapToTicks(true);
		label.setHorizontalAlignment(JLabel.CENTER);
		gbc.fill = GridBagConstraints.HORIZONTAL;
		gbc.anchor = GridBagConstraints.CENTER;
		gbc.weightx = 0.8;
		gbc.weighty = 0;
		gbc.gridwidth = 1;
		gbc.gridheight = 1;
		gbc.gridx = 0;
		gbc.gridy = 2;
		gbc.insets = new Insets(0, 6, 6, 6);
		
		add(slider, gbc);*/

		
		
		// ---------- Page Divider
		
		JSeparator pageDivider = new JSeparator(SwingConstants.VERTICAL);
		
		gbc.fill = GridBagConstraints.VERTICAL;
		gbc.weightx = 0;
		gbc.weighty = 1.0;
		gbc.gridwidth = 1;
		gbc.gridheight = 10;
		gbc.gridx = 2;
		gbc.gridy = 0;
		gbc.insets = new Insets(2, 0, 2, 0);
		
		add(pageDivider, gbc);

		// ---------- Right-Hand-Side Canvas
		
		gbc.fill = GridBagConstraints.BOTH;
		gbc.weightx = 3.0;
		gbc.weighty = 10.0;
		gbc.gridwidth = 3;
		gbc.gridheight = 10;
		gbc.gridx = 3;
		gbc.gridy = 0;
		gbc.insets = new Insets(6, 6, 6, 6);
		
		//canvas.setPreferredSize(new Dimension(300, 300));
		canvas.setBackground(Color.decode("#F4F4F4"));
		canvas.setBorder(BorderFactory.createEtchedBorder(EtchedBorder.LOWERED));
		
		add(canvas, gbc);
	}
	
	private void RunTextField(JTextField field){
		canvas.clear();
		NewShapeCommand command = new NewShapeCommand();
		command.execute(field.getText());
		place(command.getShapes());
	}
}
