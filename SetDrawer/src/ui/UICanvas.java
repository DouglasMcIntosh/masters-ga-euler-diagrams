package ui;

import java.awt.Graphics;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JPanel;

import interfaces.DrawableShape;

public class UICanvas extends JPanel {
	public List<DrawableShape> shapes;
	public UICanvas() {
		shapes = new ArrayList<DrawableShape>();
		
	}
	
	// Pop the shape onto the list. Next time the canvas is redrawn ("paint()" is called)
	// it'll draw all currently stored shapes.
	public void place(DrawableShape shape) {
		shapes.add(shape);
	}
	
	@Override
	public void paint(Graphics graphics) {
		super.paint(graphics);
		
		for(DrawableShape shape : shapes) {
			shape.draw(graphics);
		}
	}
	
	public void clear() {
		shapes.clear();
	}
}
