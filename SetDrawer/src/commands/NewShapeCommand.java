package commands;

import java.util.ArrayList;
import java.util.List;

import interfaces.DrawableShape;
import interfaces.DrawableShapeController;
import mocks.MockDrawableController;
import parsers.TextInputParser;

public class NewShapeCommand {
	private DrawableShapeController controller;
	private List<DrawableShape> shapes;
	
	public List<DrawableShape> getShapes(){
		return shapes;
	}
	
	public NewShapeCommand() {
		shapes = new ArrayList<DrawableShape>();
		controller = new MockDrawableController();
	}
	
	public void execute(String info) {
		// If there is no info being passed just return an empty list
		if(info.length() == 0 || info == "" || !info.contains(","))
			return;
		controller.createList(info);
		shapes = controller.getShapes();
	}
}
