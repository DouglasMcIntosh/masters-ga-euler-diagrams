/**
 * 
 */
package interfaces;

import java.util.List;

public interface DrawableShapeController {
	public void createList();
	public void createList(String info);
	public void createList(int numOfShapes);
	public List<DrawableShape> getShapes();
}
