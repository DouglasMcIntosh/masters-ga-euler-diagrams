package interfaces;

import java.awt.Graphics;

public interface DrawableShape
{
	public void draw(Graphics graphics);
}
