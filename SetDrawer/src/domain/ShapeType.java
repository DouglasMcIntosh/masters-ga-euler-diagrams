package domain;

public enum ShapeType {
	CIRCLE,
	OVAL,
	SQUARE,
	OVAL_DISPLAY,
	DEFAULT
}
