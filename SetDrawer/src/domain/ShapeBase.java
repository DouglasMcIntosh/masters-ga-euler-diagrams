package domain;

public class ShapeBase {
	protected int x;
	protected int y;
	protected ShapeType type;
	protected int width;
	protected int height;
	
	public int getX() {
		return x;
	}
	public int getY() {
		return y;
	}
	public ShapeType getType() {
		return type;
	}
	
	public int getWidth() {
		return width;
	}
	public int getHeight(){
		return height;
	}
	public ShapeBase() {
		this(0, 0, ShapeType.DEFAULT);
	}
	public ShapeBase(int x, int y, ShapeType type) {
		this.x = x;
		this.y = y;
		this.type = type;
	}
}
