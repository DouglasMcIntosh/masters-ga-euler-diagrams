package mocks;

import java.awt.Color;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import drawableObjects.DrawableOval;
import interfaces.DrawableShape;
import interfaces.DrawableShapeController;
import parsers.TextInputParser;

public class MockDrawableController implements DrawableShapeController {
	private List<DrawableShape> shapes;
	private TextInputParser parser;
	private Random rng;
	
	public MockDrawableController() {
		shapes = new ArrayList<DrawableShape>();
		parser = new TextInputParser();
		rng = new Random(System.currentTimeMillis());
	}
	
	@Override
	public void createList() {
		createList(5);
	}

	@Override
	public List<DrawableShape> getShapes() {
		return shapes;
	}
	
	@Override
	public void createList(int numOfShapes) {
		for(int i = 0; i < numOfShapes; i++) {
			shapes.add(createOval());
		}
	}
	
	@Override
	public void createList(String info) {
		parser.setInputData(info);
		List<String> shapeNames = parser.getPoints();
		// At this point we have a list with the shape 'names' in it 
		// Just find out which ones have commonality between them and sorted
		// For ease of use ive just flung out the number
		createList(shapeNames.size());
	}
	
	private DrawableOval createOval() {
		Color outlineColor = new Color(rng.nextInt(256),rng.nextInt(256),rng.nextInt(256),255);
		int x = rng.nextInt(251);
		int y = rng.nextInt(251);
		int width = 75;
		int height = 75;
		int rotation = rng.nextInt(91);
		return new DrawableOval(x, y, width, height, rotation, outlineColor);
	}
}
