package parsers;

import java.util.ArrayList;
import java.util.List;

public class TextInputParser {
	private String inputData;
	
	public void setInputData(String inputData) {
		this.inputData = inputData;
	}
	
	public TextInputParser() {
		this("");
	}
	
	public TextInputParser(String inputData) {
		this.inputData = inputData;
	}
	
	public List<String> getPoints(){
		String[] points = inputData.split(",");
		List<String> result = new ArrayList<String>();
		for(int i = 0; i < points.length; i++) {
			result.add(points[i]);
		}
		return result;
	}
}
