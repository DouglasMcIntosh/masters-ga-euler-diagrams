package ui;

import java.awt.BasicStroke;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.geom.AffineTransform;
import java.util.ArrayList;

import javax.swing.JPanel;

public class UICanvas extends JPanel {
	ArrayList<Oval> ovals;
	public UICanvas() {
		ovals = new ArrayList<Oval>();
		
	}
	
	public void place(Oval oval) {
		ovals.add(oval);
	}
	
	@Override
	public void paint(Graphics graphics) {
		super.paint(graphics);
		Graphics2D graphics2D = (Graphics2D)graphics;
		
		// Make the circle smooth - add antialiasing
		graphics2D.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
		AffineTransform oldTransform;
		
		int originX, originY;
		for (Oval oval : ovals) {
			originX = oval.x - (oval.width / 2);
			originY = oval.y - (oval.height / 2);
			
			// In order to not stack translations/rotations, we have to store the default transform
			// Then re-apply it at the end of the operation
			// This resets all the translations/rotations we've done on this shape
			// So the next shape is drawn at a null transform
			oldTransform = graphics2D.getTransform();
			// Draw the see-through (soft, gooey centre) portion of the oval
			graphics2D.setColor(oval.fillColor);
			
			// Java paints on a transform matrix and considers the top-left of a shape as its origin
			// Therefore, we need to move the shape back to the real origin (centre of the shape) before rotation
			// then reset the position afterwards
			// Also, we cannot paint a shape AT a position - that entire distance vector would be rotated too
			// Instead, we have to move the entire drawing board to the shape's origin, then draw the shape at the new origin
			graphics2D.translate(originX, originY);
			graphics2D.translate(oval.width / 2.0, oval.height / 2.0);
			graphics2D.rotate(Math.toRadians(oval.rotation));
			graphics2D.translate(-oval.width / 2.0, -oval.height / 2.0);
			
			graphics2D.fillOval(0, 0, oval.width, oval.height);

			// Draw the outline (crumbly crust) of the oval
			graphics2D.setStroke(new BasicStroke(2));
			graphics2D.setColor(oval.outlineColor);
			graphics2D.drawOval(0, 0, oval.width, oval.height);
			graphics2D.setTransform(oldTransform);
		}
	}
}
